const API_URL= 'https://localhost:5050';

document.addEventListener('DOMContentLoaded', function () {
    if(localStorage.getItem("accessToken") != null) {
        window.open('todo.html', '_self');
    }
    if (localStorage.getItem("tmp_userName") != null) {
        document.getElementById("input_email").value = localStorage.getItem("tmp_userName");
        localStorage.removeItem('tmp_userName');
    }
    document.getElementById("input_email").focus();
});

function Register() {
    fetch(`${API_URL}/api/reg`, {
        method: 'POST',
        json: true, 
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            username: document.getElementsByName("login")[0].value,
            password: document.getElementsByName("password")[0].value,
            firstName: document.getElementsByName("firstname")[0].value,
            lastName: document.getElementsByName("lastname")[0].value,
            email: document.getElementsByName("email")[0].value
        })
    }).then((response) => {
        switch (response.status) {
            case 200: {
                alert("Registration is successfull, redirecting You to authorization page...");
                window.open('index.html', '_self');
                break;
            }
            case 400: {
                if (confirm("User is already registered! You`ll be redirected to authorization page")) {
                    window.open('index.html', '_self');
                }
                break;
            }
            default: {
                alert("Some type of error");
                break;
            }
        }
    }).catch((error) => {
        alert(error);
    });
}

function Login() {
    if(document.getElementById("input_email").value == "" || document.getElementById("input_password").value == "") {
        alert("Input your authentication data!");
        document.getElementById("input_email").value = "";
        document.getElementById("input_password").value = "";
        document.getElementById("input_email").focus();
        return;
    }
    fetch(`${API_URL}/api/auth`, {
        method: 'POST',
        json: true, 
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            email: document.getElementById("input_email").value,
            password: document.getElementById("input_password").value,
            returnUrl: null,
            ExternalLogins: null
        })
    }).then((response) => response.json()).then((data) => {
        if (!data.access_token) {
            if (confirm("Wrong Username or Password. You`ll be redirected to registration page")) {
                if (document.getElementById("input_email").value != "") {
                    localStorage.setItem("tmp_userName", document.getElementById("input_email").value);
                }
                window.open('reg.html', '_self');
            }
            document.getElementById("input_email").value = "";
            document.getElementById("input_password").value = "";
            document.getElementById("input_email").focus();
        } else {    
            localStorage.setItem("accessToken", data.access_token);
            localStorage.setItem("userName", data.username);
            window.open('todo.html', '_self');
        }
    }).catch((error) => {
        alert(error);
    });
}

function Google_Login() {
    fetch(`${API_URL}/api/google`, {
        method: 'POST',
        json: true, 
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            provider: "Google",
            returnUrl: "https://localhost:8080/index.html"
        })
    }).then((response) => response.json()).then((data) => {
        console.log(data);
    }).catch((error) => {
        alert(error);
    });
}

function clickPress(event) {
    if (event.keyCode == 13 && event.target.id == "input_password") {
        Login();
    } else if (event.keyCode == 13) {
        document.getElementById("input_password").focus();
    }
}