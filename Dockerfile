FROM nginx:latest
COPY ["todo.html", "reg.html", "login.js", "Index.js", "index.html", "index.css", "etc/nginx/html/"] 
COPY configuration/nginx.conf /etc/nginx
COPY configuration/nginx-selfsigned.key /etc/ssl/private
COPY ["configuration/nginx-selfsigned.crt", "configuration/dhparam.pem", "/etc/ssl/certs"]