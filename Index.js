const API_URL= 'https://localhost:5050';

let controller; //refactoring

document.addEventListener('DOMContentLoaded', function () {
    if (localStorage.getItem("userName") == null) {
        window.open('index.html', '_self');
        return;
    }
    ApplicationStart();
    document.getElementById("username").innerHTML = "Welcome, " + localStorage.getItem("userName");
});

function ApplicationStart() {
    let view = new View();
    let model = new Model();
    controller = new Controller(view, model);

    controller.getAll();
}

class Model {
    todoItems = [];
    constructor(guid, id, description, deadline, status) {
        this.guid = guid;
        this.id = id;
        this.description = description;
        this.deadline = deadline;
        this.status = status;
    }
}

class View {
    async renderTodoItems() {
        try {
            const data = await fetch(`${API_URL}/api/todo`, {
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    "Authorization": 'Bearer ' + localStorage.getItem("accessToken")
                  }
            });
            const json = await data.json();
            json
                .sort((a, b) => a - b)
                .forEach(element => {
                    insertData(element);
                });
            return json;
        } catch(err) {
            console.log(err);
            alert('ERROR WHEN LOADING DATA');
        }
    }
}

class Controller {
    constructor(view, model){
        this.view = view;
        this.model = model;
    }

    async getAll() {
        await this.view.renderTodoItems().then(response => {
            this.model.todoItems = response;
        });
    }

    updateOrAdd(id) {
        let index = this.model.todoItems.findIndex((el) => el.id === parseInt(document.getElementsByName("id")[id].value));
        if (index === -1) {
            this.addItem(id);
        } else {
            this.changeItem(id);
        }
    }

    addItem(id) {
        fetch(`${API_URL}/api/todo`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json;charset=utf-8',
              "Authorization": 'Bearer ' + localStorage.getItem("accessToken")  // передача токена в заголовкi
            },
            body: JSON.stringify({
                id: parseInt(document.getElementsByName("id")[id].value),
                description: document.getElementsByName("description")[id].value,
                deadline: document.getElementsByName("deadline")[id].value,
                status: document.getElementsByName("status")[id].value == "true",
            })
        }).then((response) => {
            while (document.getElementById("myTable").rows.length > 1) {
                document.getElementById("myTable").deleteRow(-1);
            }
            this.getAll();
        }).catch((error) => {
            console.log(error);
        });
    }

    changeItem(id) {
        fetch(`${API_URL}/api/todo/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                "Authorization": 'Bearer ' + localStorage.getItem("accessToken")
            },
            body: JSON.stringify({
                guid: document.getElementsByName("guid")[id].value,
                id: parseInt(document.getElementsByName("id")[id].value),
                description: document.getElementsByName("description")[id].value,
                deadline: document.getElementsByName("deadline")[id].value,
                status: document.getElementsByName("status")[id].value == "true"
            })
        }).then(response => {
            while (document.getElementById("myTable").rows.length > 1) {
                document.getElementById("myTable").deleteRow(-1);
            }
            this.getAll();
        }).catch((error) => {
            console.log(error);
        });
    }

    deleteItem(id) {
        let index = this.model.todoItems.findIndex((el) => el.id === parseInt(document.getElementsByName("id")[id].value));
        if (index != -1) {
            fetch(`${API_URL}/api/todo/${parseInt(document.getElementsByName("id")[id].value)}`, {
                method: 'DELETE',
                headers: {
                  'Content-Type': 'application/json;charset=utf-8',
                  "Authorization": 'Bearer ' + localStorage.getItem("accessToken")
                },
            }).then((response) => {
                switch (response.status) {
                    case 403: {
                        alert("You don`t have permission for that!");
                        break;
                    }
                    default: {
                        while (document.getElementById("myTable").rows.length > 1) {
                            document.getElementById("myTable").deleteRow(-1);
                        }
                        this.getAll();
                        break;
                    }
                }
            }).catch((error) => {
                console.log(error);
            });
        } else {
            document.getElementById("myTable").deleteRow(-1);
        }
    }
}

function addRow(id) {
    let currentRow = myTable.insertRow(-1), 
    currentCell = currentRow.insertCell(-1), 
    input0 = document.createElement("input");
    if (id) {
        currentRow.classList.add('id-'+id);
    }   
    input0.setAttribute("readonly", "readonly");
    input0.setAttribute("name", "guid");
    input0.setAttribute("style", "text-align: center; width: 19em;");
    currentCell.setAttribute("style", "border-color: transparent; text-align: center;");
    currentCell.appendChild(input0);

    let input1 = document.createElement("input");
    input1.setAttribute("readonly", "readonly");
    input1.setAttribute("value", id);
    input1.setAttribute("name", "id");
    currentCell = currentRow.insertCell(-1)
    input1.setAttribute("style", "width: 4.5em; text-align: center;");
    currentCell.setAttribute("style", "border-color: transparent; text-align: center;");
    currentCell.appendChild(input1);

    let input2 = document.createElement("input");
    input2.setAttribute("type", "text");
    input2.setAttribute("name", "description");
    currentCell = currentRow.insertCell(-1);
    currentCell.setAttribute("style", "border-color: transparent; text-align: center;");
    currentCell.appendChild(input2);

    let input3 = document.createElement("input");
    input3.setAttribute("type", "date");
    input3.setAttribute("name", "deadline");
    currentCell = currentRow.insertCell(-1);
    currentCell.setAttribute("style", "border-color: transparent;");
    currentCell.appendChild(input3);

    let select = document.createElement("select");
    select.setAttribute("name", "status");
    select.append(new Option("In Progress", "true")),
    select.append(new Option("Done", "false"));
    currentCell = currentRow.insertCell(-1);
    currentCell.setAttribute("style", "border-color: transparent;");
    currentCell.appendChild(select);
    
    let btn1 = document.createElement("button");
    btn1.innerHTML = "Update";
    btn1.setAttribute("id", id);
    btn1.addEventListener('click', () => {
        controller.updateOrAdd(currentRow.rowIndex - 1);
    });
    currentCell = currentRow.insertCell(-1);
    currentCell.setAttribute("style", "border-color: transparent;");
    currentCell.appendChild(btn1);

    let btn2 = document.createElement("button");
    btn2.innerHTML = "Delete";
    btn2.setAttribute("id", id); 
    btn2.addEventListener('click', () => {
        controller.deleteItem(currentRow.rowIndex - 1);
    });
    currentCell = currentRow.insertCell(-1);
    currentCell.setAttribute("style", "border-color: transparent;");
    currentCell.appendChild(btn2);
}

function insertData(element) {
    addRow(element.id);
    const row = document.querySelector(`tr.id-${element.id}`);
    row.querySelector('[name="guid"]').setAttribute("value", element.guid);
    row.querySelector('[name="id"]').setAttribute("value", element.id);
    row.querySelector('[name="description"]').setAttribute("value", element.description);
    row.querySelector('[name="deadline"]').setAttribute("value", element.deadline);
    row.querySelector('[name="status"]').getElementsByTagName('option')[+(!element.status)].selected = 'selected';
}

function LogOut() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('userName');
    window.open('index.html', '_self');
}